#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=e

band=hi

rawdata=/rawdata/eht/rev7/FITS/trackE/hi

dockertag=3e39cd9682414a5e770ad64c534afc3b06a69de6_b85149ec63a357537fc4955f4920ed7147c482da

### input for the machine that has to process the data ###
workdir0=/scratch2/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=/DATA/mjanssen/EHT_casa_processed_data/rev5/

### input to control memory usage ###
maxcores=8

mincores=2



singularity_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
