#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=d

band=lo

rawdata=/vol/astro4/eht/mjanssen/raw/e17-rev5/trackD/lo

dockertag=3e39cd9682414a5e770ad64c534afc3b06a69de6_b85149ec63a357537fc4955f4920ed7147c482da

### input for the machine that has to process the data ###
workdir0=/scratch/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=mjanssen@coma01.science.ru.nl:/vol/astro4/eht/mjanssen/EHT-processed-data/rev5

### input to control memory usage ###
maxcores=11GB

mincores=50GB



singularity_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
