#!/bin/bash
# As data_production.sh but it uses the local rPICARD installation instead of singularity.
set -e

### input for the data that is to be processed ###
year=2017

track=e

band=lo

rawdata=/vol/astro4/eht/mjanssen/raw/e17-rev7/trackE/lo

dockertag=test_local_rPICARDv4.0.1

### input for the machine that has to process the data ###
workdir0=/scratch/mjanssen

### input for the machine that will store the processed data ###
### set to workdir0 to keep the data there ###
longtermstorage0=${workdir0}

### input to control memory usage ###
maxcores=11GB

mincores=50GB



local_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
