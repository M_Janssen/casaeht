#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=c

band=hi

rawdata=/vol/astro4/eht/mjanssen/raw/e17-rev7/trackC/hi/

dockertag=69b38aabc1bb832d4a170ff20da0392b127fdf42_71568183497ab769bca9ab8c2b6442140a9568e0

### input for the machine that has to process the data ###
workdir0=/scratch0/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=$workdir0

### input to control memory usage ###
maxcores=11GB

mincores=50GB



singularity_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
