#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=c

band=hi

rawdata=/vol/astro4/eht/mjanssen/raw/e17-rev7/trackC/hi

dockertag=30e6ca14fb50275013c668285a3b476f9bc85436_91da63236db34f3a31b5309b18ac159128f28a35

### input for the machine that has to process the data ###
workdir0=/scratch/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=mjanssen@coma01.science.ru.nl:/vol/astro4/eht/mjanssen/EHT-processed-data

### input to control memory usage ###
maxcores=11GB

mincores=50GB



singularity_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
