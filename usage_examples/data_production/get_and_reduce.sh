#!/bin/bash
#Must have casaeht/bin in PATH.
#Example script for data download from correlator portal + singularity data production for all days, combining bands.
#Everything runs on a single machine.
set -e


### input for the data that is to be processed ###
year=2018

month=April

correlatorv=Rev0

alltracks=( a24 c21 c25 d28 e22 g27 )

dockertag=30e6ca14fb50275013c668285a3b476f9bc85436_f85e8d543243c26dff1767c8fa5085c4ed368b0d

### directory for raw data ###
rawdata=/rawdata/eht/${year}${month}/${correlatorv}

### working directory ###
workdir0=/scratch/mjanssen

### storing directory for processed data ###
longtermstorage0=/DATA/mjanssen/EHT_casa_processed_data/${year}${month}/${correlatorv}

### input to control memory usage ###
maxcores=11GB

mincores=50GB



### exec ###
mkdir -p $workdir0
mkdir -p $longtermstorage0
get_data ${year}${month} ${correlatorv}-Cal ${rawdata}/cal
get_data ${year}${month} ${correlatorv}-Sci ${rawdata}/sci

for track in "${alltracks[@]}"; do
    singularity_data_production $year $track allbands $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores unorganized
done
