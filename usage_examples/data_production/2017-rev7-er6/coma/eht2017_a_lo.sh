#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=a

band=lo

rawdata=/vol/astro2/users/mjanssen/raw/e17-rev7/trackA/lo

dockertag=99269c2cd5a55d5ffddebb93ccf8c4d442bb264a_d9c2d56f929d154b5ad360b1a0d179baf89d1be9

### input for the machine that has to process the data ###
maxcores=11GB

mincores=50B

workdir0=/scratch/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=mjanssen@coma01.science.ru.nl:/vol/astro4/eht/mjanssen/EHT-processed-data



### exec part ###
processing_folder=eht${year}_${track}_${band}
workdir=${workdir0}/${processing_folder}
longtermstorage=${longtermstorage0}/${processing_folder}

mkdir -p $workdir
cd $workdir
singularity build casavlbi.pipe docker://mjanssen2308/casavlbi_ehtproduction:$dockertag
singularity exec --cleanenv -B $workdir:/data -B $rawdata:/rawdata ./casavlbi.pipe process_eht /data --maxcores $maxcores --mincores $mincores --year $year --track $track --band $band --rawdata /rawdata

process_eht $workdir --rsync $longtermstorage --noms --clean --dry
