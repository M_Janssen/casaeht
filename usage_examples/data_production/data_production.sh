#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=a

band=lohi

rawdata=/path/to/the/raw/data

dockertag=65a43e0f74485712be1639cbccbc9c48dc4406c1_f8dfd149cd28b44d89f73079d93e36674663bdac

### input for the machine that has to process the data ###
workdir0=/path/to/your/working/dir

### input for the machine that will store the processed data ###
### set to workdir0 to keep the data there ###
longtermstorage0=${workdir0}

### input to control memory usage ###
maxcores=33GB

mincores=50GB



singularity_data_production $year $track $band $dockertag $workdir0 $longtermstorage0 $rawdata $maxcores $mincores
