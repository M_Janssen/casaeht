#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import math

#python p_slurm.py <workdir with all input set up> <total number of VLBI scans in the data set>

if len(sys.argv) != 3:
    raise OSError('Wrong number of input arguments. Must be 2.')
wdir = sys.argv[1]


#Cluster specific:
MEM_PER_NODE = 240
CPU_PER_NODE = 72
P_TO_CASAEHT = '/u/mjanssen/casaeht'


#rpicard processing specific:
MEM_PER_SCAN   = 80
SLRUM_TEMPLATE = """#!/bin/bash

#SBATCH --nodes={0}
#SBATCH --ntasks-per-node={2}
#SBATCH --mem={3}GB
#SBATCH --time=1-00:00:00

#SBATCH --mail-type=ALL
#SBATCH --mail-user=mjanssen@mpifr-bonn.mpg.de

scontrol show hostname > mpi_host_file.tmp
sed 's/$/ slots={1}/' mpi_host_file.tmp > mpi_host_file
rm mpi_host_file.tmp
mv mpi_host_file {4}/input/

cd {4}
picard -pdq {5} >> picard.out""" #--nodes, --ntasks-per-node-for-rpicard, --ntasks-per-node-for-slurm, MEM_PER_NODE, <workdir with all input set up>, <rpicard -q step>
CAL_TO_SCI_CLEANUP = """
rm -fr calibration_tables/ff_mb_cal_s.t
rm -f fringe_params.stored.cal*
rm -f scan_refants.stored.cal*
python {0}/supporting_scripts/all_sci.py input
""".format(P_TO_CASAEHT)


global fiter
fiter = 0
def w_slurmf(w_str, step):
    global fiter
    fname = 's'+str(fiter)+'_'+step+'.sh'
    with open(fname, 'w', encoding='utf-8') as f:
        f.write(w_str)
    fiter += 1
    os.system('chmod +x {0}'.format(fname))
    return fname


#Run pipeline steps in sequence, with each step finishing within <24h.
#Generation of slurm scripts:
scans_per_node = int(MEM_PER_NODE/MEM_PER_SCAN)
N_nodes        = int(math.ceil(int(sys.argv[2]) / scans_per_node))
sbatch_flist   = []
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, CPU_PER_NODE, CPU_PER_NODE, MEM_PER_NODE, wdir, 'c'), 'c'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, CPU_PER_NODE, CPU_PER_NODE, MEM_PER_NODE, wdir, '4'), '4'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/6), 2*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '5'), '5' #calib: fewer and shorter scans
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/6), 2*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '6'), '6'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, CPU_PER_NODE, CPU_PER_NODE, MEM_PER_NODE, wdir, '7') + CAL_TO_SCI_CLEANUP, '7'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, CPU_PER_NODE, MEM_PER_NODE, MEM_PER_NODE, wdir, '8'), '8'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '13.0'), '13.0'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node,CPU_PER_NODE, MEM_PER_NODE, wdir, '13.1'), '13.1'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '13.2'), '13.2'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '13.3'), '13.3'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(N_nodes, scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '14'), '14'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '15'), '15'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(int(N_nodes/3), 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, '16'), '16'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, CPU_PER_NODE, CPU_PER_NODE, MEM_PER_NODE, wdir, '0,1'), '0,1'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, 3*scans_per_node, CPU_PER_NODE, MEM_PER_NODE, wdir, 'h,i,l'), 'h,i,l'
                            )
                   )
sbatch_flist.append(w_slurmf(SLRUM_TEMPLATE.format(1, 2, 2, MEM_PER_SCAN, wdir, 'k'), 'k'
                            )
                   )

thisID = os.popen('sbatch '+sbatch_flist[0]).read().rstrip('\n').split()[-1]
for submit in sbatch_flist[1:]:
    thisID = os.popen('sbatch --dependency=afterok:'+thisID + ' ' + submit).read().rstrip('\n').split()[-1]
