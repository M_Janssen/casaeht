#!/bin/bash
#Must have casaeht/bin in PATH.
set -e

### input for the data that is to be processed ###
year=2017

track=e

band=hi

rawdata=/vol/astro4/eht/mjanssen/raw/e17-rev7/trackE/hi

dockertag=fac8ec7c292e8956f47fccd4e5f2d522b317245d_281ff01126bc1bf365eccae13985bc57e93016e8

### input for the machine that has to process the data ###
maxcores=11GB

mincores=20GB

workdir0=/scratch1/mjanssen

### input for the machine that will store the processed data ###
longtermstorage0=mjanssen@coma01.science.ru.nl:/vol/astro4/eht/mjanssen/EHT-processed-data



### exec part ###
processing_folder=eht${year}_${track}_${band}
workdir=${workdir0}/${processing_folder}
longtermstorage=${longtermstorage0}/${processing_folder}

export SINGULARITY_TMPDIR=${workdir0}/singularity.tmp
export SINGULARITY_CACHEDIR=${workdir0}/singularity.cache
mkdir -p ${SINGULARITY_TMPDIR}
mkdir -p ${SINGULARITY_CACHEDIR}

mkdir -p $workdir
cd $workdir
singularity build casavlbi.pipe docker://mjanssen2308/casavlbi_ehtproduction:$dockertag
singularity exec --cleanenv -B $workdir:/data -B $rawdata:/rawdata ./casavlbi.pipe process_eht /data --maxcores $maxcores --mincores $mincores --year $year --track $track --band $band --rawdata /rawdata

process_eht $workdir --rsync $longtermstorage --noms --clean --dry
