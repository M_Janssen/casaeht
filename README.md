Rationale:  
=========  
Bundled configuration instructions, input files, and supporting scripts for the processing of EHT data with rPICARD.  
  
Usage:  
======   
- bin/get_data to download raw EHT data from the correlator portal.  
- bin/hops_to_casa_flags.py to convert a list of HOPS flags to CASA flags.  
- bin/process_eht to process the raw data.  
  
Instructions and examples are given in the scripts.  
Scripts for Singularity-based data production runs are in usage_examples/data_production.  
  
To calibrate the publicly archived L1 EHT data, (https://eventhorizontelescope.org/for-astronomers/data), you have to extract all of the fits files and organize them by observing track (a,b,c,d,e in 2017) into /rawdata/ directories. For every track, you can then run the casaeht/usage_examples/data_production/data_production.sh script with the following edits:  
- track=<name of the track, e.g., a,b,c,d, or e for the 2017 data>  
- rawdata=<path to your /rawdata/ directory for the particular track>  
- dockertag=<tag of the rPICARD container version you wish to use from https://hub.docker.com/r/mjanssen2308/casavlbi_ehtproduction>  
- workdir0=<path to a working directory, where rPICARD will run>  

Requirements:  
=============  
For Singularity-based data production runs:  
- https://singularity.lbl.gov.  
For interactive use:  
- https://bitbucket.org/M_Janssen/picard.  
- https://github.com/achael/eht-imaging (optional for bin/process_eht --diagnostics).  
- https://virtualenv.pypa.io (optional for bin/process_eht --diagnostics).  
