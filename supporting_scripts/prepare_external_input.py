"""
Helper functions to prepare CASA calibration tables from a priori information.
"""
import casatools as casac
#from casac import casac

def change_caltb_nsname(caltb, new_msname):
    mytb = casac.table()
    mytb.open(caltb, nomodify=False)
    mytb.putkeyword('MSName', new_msname)
    mytb.flush()
    mytb.done()
    mytb.clearlocks()
