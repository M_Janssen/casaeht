"""
Closure phase selection method copied from eht-imaging/ehtim/plotting/summary_plots.py.
"""
import matplotlib
matplotlib.use('Agg')
import os
import sys
import glob
import ehtim
from optparse import OptionParser


def main():
    """
    Calls plot_closure_phases() with a uvfits-file from the command line.
    """
    usage  = "%prog uvfits-file [-t <TIME AVG>] [-o <OUTPATH NAME>] [-h or --help]"
    parser = OptionParser(usage=usage)
    parser.add_option("-t", type="string",dest="timeavg",default="scan",
                      help=r"Time averaging intervals of data in seconds or over each scan. [Default: %default]")
    parser.add_option("-o", type="string",dest="outpath",default="",
                      help=r"Directory where plots are saved. [Default: %default (pwd)]")
    (opts,args) = parser.parse_args()
    if len(sys.argv)<2:
        parser.print_help()
        sys.exit(1)
    infiles = glob.glob(args[0])
    for uvf in infiles:
        try:
            plot_closure_phases(uvf, opts.timeavg, opts.outpath)
        except:
            pass


def plot_closure_phases(uvfits, timeavg='', outpath='', uvf_folder=True):
    obs = ehtim.obsdata.load_uvfits(uvfits)
    if timeavg=='scan':
        obs.add_scans()
        obs = obs.avg_coherent(1,scan_avg=True)
    elif timeavg:
        obs = obs.avg_coherent(float(timeavg))
    cp                = obs.c_phases(mode="all", count="max")
    n_cphase          = len(cp)
    alltris           = [(str(cpp['t1']),str(cpp['t2']),str(cpp['t3'])) for cpp in cp]
    uniqueclosure_tri = []
    for tri in alltris:
        if tri not in uniqueclosure_tri: uniqueclosure_tri.append(tri)
    uniqueclosure_tri_sorted  = sorted([sorted(tri) for tri in uniqueclosure_tri])
    uniqueclosure_tri_sorted2 = []
    for tri in uniqueclosure_tri_sorted:
        if tri not in uniqueclosure_tri_sorted2: uniqueclosure_tri_sorted2.append(tri)
    for i,tri in enumerate(uniqueclosure_tri_sorted2):
        plotfile = ''
        if outpath:
            plotfile += outpath + '/'
        if uvf_folder:
            plotfile = os.path.splitext(uvfits)[0]+'_cphases/'
            if not os.path.exists(plotfile):
                os.makedirs(plotfile)
        plotfile += 'uvfits_cphase{0}_{1}-{2}-{3}.png'.format(str(i), tri[0], tri[1], tri[2])
        obs.plot_cphase(tri[0], tri[1], tri[2], show=False, export_pdf=plotfile, markersize=10)


if __name__=="__main__":
    main()
