import re
import sys
from glob import glob


def alter_line(filename, search_pattern, new_line, str_char = '#', illegal_string='--Katzenkratzbaum--', strict=True):
    f     = open(filename, 'r')
    lines = f.readlines()
    for i, l in enumerate(lines):
        if l[0] != str_char:
            if strict:
                if re.search(r'\b' + search_pattern + r'\b', l) and illegal_string not in l:
                    lines[i] = new_line
            else:
                if search_pattern in l and illegal_string not in l:
                    lines[i] = new_line
    f.close()
    f = open(filename, 'w')
    for l in lines:
        f.write(l)
    f.close()


input_folder = sys.argv[-1]
infiles      = glob(input_folder+'/*.inp')
for f in infiles:
    science_target         = None
    calibrators_instrphase = None
    calibrators_bandpass   = None
    calibrators_rldly      = None
    calibrators_dterms     = None
    with open(f, 'r') as of:
        content = of.readlines()
        for line in content:
            if line[0]=='#':
                continue
            if 'science_target' in line and '=' in line:
                science_target = line.strip('\n')
            if 'calibrators_instrphase' in line and '=' in line:
                calibrators_instrphase = line.strip('\n')
            if 'calibrators_bandpass' in line and '=' in line:
                calibrators_bandpass = line.strip('\n')
            if 'calibrators_rldly' in line and '=' in line:
                calibrators_rldly = line.strip('\n')
            if 'calibrators_dterms' in line and '=' in line:
                calibrators_dterms = line.strip('\n')
    if science_target and calibrators_instrphase and calibrators_bandpass and calibrators_rldly and calibrators_dterms:
        observation_inp = f
        break
cals = calibrators_instrphase.split('=')[-1].split(',')
cals.extend(calibrators_bandpass.split('=')[-1].split(','))
cals.extend(calibrators_rldly.split('=')[-1].split(','))
cals.extend(calibrators_dterms.split('=')[-1].split(','))
for c in cals:
    cc = c.strip()
    if cc!='None' and cc!='False' and cc not in science_target:
        science_target += ', ' + cc
# If we had <science_target = None> or <science_target = False> originally:
science_target = science_target.replace('None,', '')
science_target = science_target.replace('False,', '')
alter_line(observation_inp, 'science_target', science_target+'\n')
alter_line(observation_inp, 'calibrators_instrphase', 'calibrators_instrphase = None\n')
alter_line(observation_inp, 'calibrators_bandpass', 'calibrators_bandpass = None\n')
alter_line(observation_inp, 'calibrators_rldly', 'calibrators_rldly = None\n')
alter_line(observation_inp, 'calibrators_dterms', 'calibrators_dterms = None\n')
