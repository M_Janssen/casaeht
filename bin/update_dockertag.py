#!/usr/bin/env python
#python update_dockertag.py <path with .sh files> <dockertag>
import re
import sys
from pipe_modules.auxiliary import get_extension_matches_in_all_subdirs


def alter_line(filename, search_pattern, new_line, str_char = '#', illegal_string='--Katzenkratzbaum--', strict=False):
    f     = open(filename, 'r')
    lines = f.readlines()
    for i, l in enumerate(lines):
        if l[0] != str_char:
            if strict:
                if re.search(r'\b' + search_pattern + r'\b', l) and illegal_string not in l:
                    lines[i] = new_line
            else:
                if search_pattern in l and illegal_string not in l:
                    lines[i] = new_line
    f.close()
    f = open(filename, 'w')
    for l in lines:
        f.write(l)
    f.close()


def main():
    nl          = 'dockertag={0}\n'.format(sys.argv[-1])
    run_scripts = get_extension_matches_in_all_subdirs(sys.argv[-2], '.sh', no_duplicates=False)
    for script in run_scripts:
        alter_line(script, 'dockertag=', new_line=nl)

if __name__=="__main__":
    main()



