#!/usr/bin/env python
#
# Copyright (C) 2017 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
import sys
import datetime
from optparse import OptionParser


def main():
    usage       = "%prog hops_flagfile casa_listobsfile casa_flagoutfile year  [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)!=5:
        parser.print_help()
        sys.exit(1)
    hops_station_codes      = {}
    if args[3] == '2017':
        hops_station_codes['A'] = 'AA'
        hops_station_codes['X'] = 'AP'
        hops_station_codes['Y'] = 'SP'
        hops_station_codes['S'] = 'SM'
        hops_station_codes['R'] = 'SR'
        hops_station_codes['L'] = 'LM'
        hops_station_codes['J'] = 'JC'
        hops_station_codes['Z'] = 'AZ'
        hops_station_codes['P'] = 'PV'
    else:
        hops_station_codes['A'] = 'AA'
        hops_station_codes['X'] = 'AX'
        hops_station_codes['Y'] = 'SZ'
        hops_station_codes['S'] = 'SW'
        hops_station_codes['R'] = 'SR'
        hops_station_codes['L'] = 'LM'
        hops_station_codes['J'] = 'MM'
        hops_station_codes['Z'] = 'MG'
        hops_station_codes['P'] = 'PV'
        hops_station_codes['G'] = 'GL'
        hops_station_codes['N'] = 'NN'
        hops_station_codes['K'] = 'KP'
    monthsnumbers           = {}
    monthsnumbers['Jan']    = '01'
    monthsnumbers['Feb']    = '02'
    monthsnumbers['Mar']    = '03'
    monthsnumbers['Apr']    = '04'
    monthsnumbers['May']    = '05'
    monthsnumbers['Jun']    = '06'
    monthsnumbers['Jul']    = '07'
    monthsnumbers['Aug']    = '08'
    monthsnumbers['Sep']    = '09'
    monthsnumbers['Oct']    = '10'
    monthsnumbers['Nov']    = '11'
    monthsnumbers['Dec']    = '12'
    hops_to_casa_flags(args[0], args[1], args[2], hops_station_codes, monthsnumbers)


def hops_to_casa_flags(hops_flagfile, casa_listobsfile, casa_flagoutfile, station_dict, monthsnumbers):
    """
    Limitations:
        - Last scan in CASA listobs file cannot be across days.
        - Flags and data must all be in the same year.
    """
    debug           = False
    scantimes_start = {}
    scantimes_end   = {}
    scanblockID     = 'nRows'
    scan            = 0
    with open(casa_listobsfile, 'r') as listf:
        scanblock = False
        all_lines = listf.readlines()
        for i,line in enumerate(all_lines):
            if scanblock and scanblockID in line:
                break
            if scanblock:
                thisline  = line.split()
                starttime = thisline[0]
                endtime   = thisline[2]
                if '/' in starttime:
                    starttime_parts = starttime.split('/')
                    current_date    = starttime_parts[0]
                    starttime       = starttime_parts[1]
                    this_year       = int(current_date.split('-')[-1])
                elif i==0:
                    print('No date found in first line:')
                    print(line)
                    print('Exiting!')
                    sys.exit(1)
                this_scanstarttime = current_date + '/' + starttime
                if endtime[:3]=='00:' and starttime[:3]=='23:':
                    date2 = all_lines[i+1].split()[0].split('/')[0]
                    if debug:
                        print('scan across days:')
                        print(line)
                        print('getting new date from next line:')
                        print(date2)
                    this_scanendtime = date2 + '/' + endtime
                else:
                    this_scanendtime = current_date + '/' + endtime
                scantimes_start[scan] = listobsdate_to_casadate(this_scanstarttime, monthsnumbers)
                scantimes_end[scan]   = listobsdate_to_casadate(this_scanendtime, monthsnumbers)
                scan += 1
            if not scanblock and scanblockID in line:
                scanblock = True
    casaf = open(casa_flagoutfile, 'w')
    with open(hops_flagfile, 'r') as hopsf:
        all_lines  = hopsf.readlines()
        flag_lines = []
        for line in all_lines:
            thisline = line.split()
            if thisline and thisline[0] != '*':
                flag_lines.append(line.split('*', 1)[0].strip().strip('\n'))
        all_flags = ''
        for flag in flag_lines:
            all_flags += ' '
            all_flags += flag
        all_flags = all_flags[1:].split('if')
        for flag in all_flags:
            if flag:
                actions = []
                if ' scan ' in flag:
                    scanID            = get_keyword_from_str(flag, 'scan')
                    start, casa_scan  = match_casa_hops_scans(scanID, scantimes_start, this_year)
                else:
                    print('did not find a scan identifier for the hops directive:')
                    print(flag)
                if ' to ' in flag:
                    endtime = hops_scanID_to_casa_timestring(this_year, flag.split('to')[1].lstrip().split()[0])
                else:
                    endtime = False
                if ' station ' in flag:
                    ant = station_dict[get_keyword_from_str(flag, 'station')]
                elif ' baseline ' in flag:
                    baseline = get_keyword_from_str(flag, 'baseline')
                    ant      = station_dict[baseline[0]] + '&' + station_dict[baseline[1]]
                else:
                    print('did not find an antenna identifier in the hops directive:')
                    print(flag)
                if endtime:
                    if ' skip ' not in flag or ' start ' in flag or ' stop ' in flag:
                        print('unknown directive for hops flagging timerange:')
                        print(flag)
                if ' start ' in flag:
                    to_start = abs(int(get_keyword_from_str(flag, 'start')))
                    actions.append((start, date_diff_by_seconds(start, to_start)))
                if ' stop ' in flag:
                    end      = scantimes_end[casa_scan]
                    from_end = abs(int(get_keyword_from_str(flag, 'stop')))
                    actions.append((date_diff_by_seconds(end, -from_end), end))
                if ' skip ' in flag:
                    if endtime:
                        end = endtime
                    else:
                        end = scantimes_end[casa_scan]
                    actions.append((start,end))
                if not actions:
                    print('unknown flagging action for hops directive:')
                    print(flag)
                else:
                    for action in actions:
                        flagcmd = """antenna='{0}' timerange='{1}~{2}' reason='from hops'\n""".format(ant, action[0], action[1])
                        casaf.write(flagcmd)
    casaf.close()


def get_keyword_from_str(instring, key):
    return instring.split(key)[1].lstrip().split()[0]


def listobsdate_to_casadate(listobsdate, monthsnumbers):
    indate1 = listobsdate.split('-')
    indate2 = indate1[-1].split('/')
    return '{0}/{1}/{2}/{3}'.format(indate2[0], monthsnumbers[indate1[1]], indate1[0], indate2[1]).split('.')[0]


def date_diff_by_seconds(indate, seconds_diff, date_format='%Y/%m/%d/%H:%M:%S'):
    new_date = datetime.datetime.strptime(indate, date_format) + datetime.timedelta(seconds=seconds_diff)
    return new_date.strftime(date_format)


def hops_scanID_to_casa_timestring(this_year, hops_timestr, outdate_format='%Y/%m/%d/%H:%M:%S'):
    hops_timestr_parts = hops_timestr.split('-')
    hops_doy           = int(hops_timestr_parts[0])
    hops_hour          = int(hops_timestr_parts[-1][0:2])
    hops_minute        = int(hops_timestr_parts[-1][2:4])
    hops_second        = int(hops_timestr_parts[-1][4:6])
    this_date          = datetime.datetime(int(this_year), 1, 1) + datetime.timedelta(hops_doy - 1)
    this_date          = this_date.replace(hour=hops_hour)
    this_date          = this_date.replace(minute=hops_minute)
    this_date          = this_date.replace(second=hops_second)
    return this_date.strftime(outdate_format)


def match_casa_hops_scans(hops_scanID, casa_scan_starttimes, this_year):
    for scan in casa_scan_starttimes.keys():
        starttime = casa_scan_starttimes[scan]
        if starttime == hops_scanID_to_casa_timestring(this_year, hops_scanID):
            return starttime, scan
    print('unmatched hops scanID:')
    print(hops_scanID, hops_scanID_to_casa_timestring(this_year, hops_scanID))
    return False, False


if __name__=="__main__":
    main()
