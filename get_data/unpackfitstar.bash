#!/bin/bash
USAGE0="This script will unpack xxxxxx-fits.tar into xxxxxx.fits"
USAGE1="Usage: $0 xxxxxx-fits.tar"
if [ $# == 0 ] ; then
    echo $USAGE0
    echo $USAGE1
    exit 1;
fi

COMMAND="mkdir -p ${1/-fits.tar/.fits}"
echo $COMMAND; $COMMAND
COMMAND="tar xvf $1 -C ${1/-fits.tar/.fits} --strip-components=1"
echo $COMMAND; $COMMAND
