import sys

infile=sys.argv[1]
with open(infile, 'r+') as f:
    lines = f.readlines()
    f.seek(0)
    for line in lines:
        lline = line.split()
        if len(lline) == 1:
            fixline = line
        else:
            fixline = lline[0]+lline[-1]+'\n'
        f.write(fixline)
    f.truncate()
