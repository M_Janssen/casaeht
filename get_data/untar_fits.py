#!/usr/bin/env python
import os
import shutil
import subprocess
from glob import glob
#copy this script together with unpackfitstar.bash to the directory where the data was downloaded and run it there

def get_extension_matches_in_all_subdirs(_directory, _extensions):
    """
    Recursively grab all files in _directory with _extensions=['.x', '.y', 'z',...]
    """
    matches = []
    for root, dirs, files in os.walk(_directory, followlinks=True):
        for filename in files:
            if filename.endswith(tuple(_extensions)):
                if _extensions in filename:
                    matches.append(os.path.join(root, filename))
    return matches

thisdir  = os.getcwd()
new_house = 'FITS'
if not os.path.exists(new_house):
    os.makedirs(new_house)
all_fits = get_extension_matches_in_all_subdirs(thisdir, 'fits.tar')
for fits in all_fits:
    untar_fits = fits.strip('-fits.tar')
    foldername = os.path.basename(fits).strip('-fits.tar') + '.fits'
    new_home   = thisdir + '/' + new_house + '/' + foldername
    if not os.path.isdir(new_home):
        print ('will untar ' + fits)
        output = subprocess.check_output(['./unpackfitstar.bash',fits])
        print (output)
        shutil.move(untar_fits+'.fits',new_house)
        os.remove(fits)
